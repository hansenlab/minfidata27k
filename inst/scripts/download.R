library(tcgaR)
getTCGA(cancer="kirp", platform="27k", idatDir="./", idat=TRUE, return=FALSE)
mappings <- tcgaR:::.getMethMappings(cancer="kirp", platform="27k")